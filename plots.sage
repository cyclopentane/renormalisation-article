#!/usr/bin/env sage
from sage.plot.scatter_plot import ScatterPlot


# Lattice potential

r = var("r")
V(r) = 1/r^12 - 1/r
r_min = derivative(V(r), r).roots()[-1][0]
print(r_min)
taylor_V(r) = taylor(V(r), r, r_min, 2)
taylor_V_2(r) = taylor(V(r), r, r_min, 4)


lj = plot(
	V(r),
	(r, 0.8, 2),
	xmin=1,
	ymax=0.3,
	ticks=[[], []],
	axes_labels=["$r$", "$V(r)$"],
	legend_label="$V(r)$",
	typeset='type1'
)
lj_taylor = plot(
	taylor_V(r),
	(r, 0.8, 2),
	color = "red",
	linestyle = "dashed",
	legend_label = "2nd order Taylor expansion"
)
lj_taylor_2 = plot(
	taylor_V_2(r),
	(r, 0.8, 2),
	color = "violet",
	linestyle = "dashed",
	legend_label = "4th order Taylor expansion"
)
plt = lj + lj_taylor # + lj_taylor_2
plt.set_legend_options(back_color=(1,1,1), shadow=False)
plt.save("lennart_jones_1.pdf")


# Phonon dispersion relation

a = 0.1
omega(k) = abs(sin(k*a/2))

k_modes = srange(-pi/a, pi/a, 2*pi/a/100)
omegas = [omega(k) for k in k_modes]

plt = scatter_plot(
	list(zip(k_modes, omegas)),
	marker = "x",
	facecolor = "blue",
	axes_labels = ["$k$", "$\omega(k)$"],
	ticks = [[-pi/a, 0, pi/a], None],
	tick_formatter=[["$-τ/2a$", "$0$", "$τ/2a$"], ""]
)

plt.set_legend_options(back_color=(1,1,1), shadow=False)
plt.save("phonon_dispersion_1.pdf")


# Phonon dispersion relation measured by quasi-physicists

ks = srange(-10, 10, 0.5)
omegas = [abs(k) + (random() - 0.5)* abs(k) * 0.1 for k in ks]

qpd1 = scatter_plot(
	list(zip(ks, omegas)),
	axes_labels = ["$k$", "$\omega(k)$"],
	marker = "x",
	facecolor = "blue",
	legend_label = "measured data"
)
qpd2 = plot(abs(r), (r, -10, 10), color = "black") #, legend_label = "linear fit")
plt = qpd1 + qpd2
plt.save("phonon_dispersion_2.pdf")
